<?php

namespace App\Controller;

use App\Entity\Journalist;
use App\Form\JournalistType;
use App\Repository\JournalistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JournalistController extends AbstractController
{
    /**
     * @Route("/journalist", name="journalist")
     */
    public function form(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $journalist = new Journalist();
        $form = $this->createForm(JournalistType::class, $journalist);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $journalist->setRoles(['ROLE_JOURNALIST']);
            $hashedPassword = $encoder->encodePassword($journalist, $journalist->getPassword());
            $journalist->setPassword($hashedPassword);
            $manager->persist($journalist);
            $manager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('journalist/FormAccount.html.twig', [
            'JournalistForm' => $form->createView()
        ]);
    }
    /**
     * @Route("/profile/{id}", name="profile_journalist")
     */
    public function index(Journalist $journalist){

        return $this->render('journalist/pageJournalist.html.twig', [
            'profil_journalist'=> $journalist
        ]);
    }

    /**
     * @Route("/journalist/{code}", name="country")
     */
    public function country(JournalistRepository $journalistRepository, string $code){

        return $this->render('NewFeed/mediaForm.html.twig', [
            'country' => $journalistRepository->findBy(["nationality"->$code]),
        ]);
    
    }

    /**
     * @Route("/modify_profile/{id}", name="modify_profile")
     */
    public function modify($id, Request $request)
    {
        
        $entityJournalist = $this->getDoctrine()->getManager();
        $pro = $entityJournalist->getRepository(Journalist::class)->find($id);

        if (!$pro) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $form = $this->createForm(JournalistType::class, $pro);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityJournalist->persist($pro);
            $entityJournalist->flush();
            return $this->redirectToRoute('profile_journalist', [
                'id' => $pro->getId()
            ]);
        }
        return $this->render('journalist/FormAccount.html.twig', [
            'JournalistForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete_profile", name="delete_profile")
     */
    public function deleteJournalist(TokenStorageInterface $tokenStorageInterface)
    {
        $entityJournalist = $this->getDoctrine()->getManager();
        $journalist = $this->getUser();
        $entityJournalist->remove($journalist);
        $entityJournalist->flush();
        $tokenStorageInterface->setToken();
        return $this->redirectToRoute('app_home');
    }

    /**
     * @Route("/add_picture", name="picture_add")
     */
    // public function addPicture(Request $request, EntityManagerInterface $manager, FileUploader $FileUploader)
    // {
    //     $journalist = new Journalist();
        
    //     $form = $this->createForm(JournalistType::class, $journalist);
    //     $form->handleRequest($request);

    //     if($form->isSubmitted() && $form->isValid()) {
    //         $imgPath = $FileUploader->upload($journalist->getImgPath());

    //         $journalist->setImgPath($imgPath);
    //         $manager->persist($journalist);
    //         $manager->flush();
    //         return $this->redirectToRoute('');
    //     }

    //     return $this->render('journalist/FormAccount.html.twig', [
    //         'form' => $form->createView()
    //     ]);
    // }

}
