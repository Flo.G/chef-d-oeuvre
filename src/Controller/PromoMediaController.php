<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PromoMediaController extends AbstractController
{
    /**
     * @Route("/promo/media", name="promo_media")
     */
    public function index()
    {
        return $this->render('promo_media/index.html.twig', [
            'controller_name' => 'PromoMediaController',
        ]);
    }
}
