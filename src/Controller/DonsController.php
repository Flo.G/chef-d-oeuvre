<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DonsController extends AbstractController
{
    /**
     * @Route("/dons", name="dons")
     */
    public function index()
    {
        return $this->render('dons/index.html.twig', [
            'controller_name' => 'DonsController',
        ]);
    }
}
