<?php

namespace App\Controller;

use App\Entity\Media;
use App\Form\MediaType;
use App\Repository\JournalistRepository;
use App\Repository\MediaRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MediasController extends AbstractController
{
    /**
     * @Route("/medias", name="medias")
     */
    public function findAll(Request $request, MediaRepository $mediaRepository)
    {
        $country = $request -> get("country");
        // $this->$mediaRepository->findAll()
        //choper l'argument country avec la Request et un ->get('country')
        

        if(!$country){    
            dump($country);    
            return $this->render('NewFeed/index.html.twig', [
            'newFeed' => $mediaRepository->findAll(),
        ]);

        }
        return $this->render('NewFeed/index.html.twig', [
            'newFeed' => $mediaRepository->findByNationality($country),
        ]);

    }

        /**
     * @Route("/form_media", name="form_media")
     */
    public function form(Request $request, EntityManagerInterface $manager, FileUploader $FileUploader)
    {
        $media = new Media();
        $form = $this->createForm(MediaType::class, $media);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mediaPath = $FileUploader->upload($media->fileVideo);
            $media->setJournalist($this->getUser());
            $media->setVideo($mediaPath);
            $manager->persist($media);
            $manager->flush();
            // return $this->redirectToRoute('profile_journalist');
        }

        return $this->render('NewFeed/mediaForm.html.twig', [
            'mediaForm' => $form->createView()
        ]);
    }

        /**
     * @Route("/modify_media/{id}", name="modify_media")
     */
    public function modify($id, Request $request)
    {
        
        $entityMedia = $this->getDoctrine()->getManager();
        $mediaform = $entityMedia->getRepository(Media::class)->find($id);

        if (!$mediaform) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $form = $this->createForm(MediaType::class, $mediaform);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityMedia->persist($mediaform);
            $entityMedia->flush();
            return $this->redirectToRoute('profile_journalist', [
                'id' => $mediaform->getId()
            ]);
        }
        return $this->render('journalist/FormAccount.html.twig', [
            'JournalistForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete_media/{id}", name="delete_media")
     */
    public function deleteMedia()
    {
        $entityMedia = $this->getDoctrine()->getManager();
        $media = $entityMedia->getRepository(Media::class)->find($this->getUser()->getId());
        $entityMedia->remove($media);
        $entityMedia->flush();
        return $this->redirectToRoute('security/login.html.twig');
    }

}