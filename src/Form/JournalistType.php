<?php

namespace App\Form;

use App\Entity\Journalist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JournalistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password')
            ->add('name')
            ->add('surname')
            ->add('age')
            ->add('nationality')
            ->add('speciality')      
            ->add('description')
            ->add('imgPath');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Journalist::class,
            'allow_extra_fields' => true
        ]);
    }
}
