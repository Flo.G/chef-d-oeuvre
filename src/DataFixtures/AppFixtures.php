<?php

namespace App\DataFixtures;

use App\Entity\Journalist;
use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(  ObjectManager $manager)
    {

        $faker = Factory::create();

                
        for ($i = 0; $i < 2; $i++) {
            $journalist = new Journalist();
            $journalist->setName($faker->name);
            $journalist->setSurname($faker->lastName);
            $journalist->setEmail($faker->email);
            $journalist->setPassword('$argon2id$v=19$m=65536,t=4,p=1$xefsJwdQx6A+PTBWwJBT0A$RlcOAYzfuB97vMvt1gPEXR64uMn9EnbReKXcHYF7X0Y');
            $journalist->setAge($faker->randomDigit);
            $journalist->setNationality($faker->countryCode);
            $journalist->setSpeciality($faker->jobTitle);
            $journalist->setImgPath($faker->imageUrl());
            $journalist->setDescription($faker->text());
            $journalist->setRoles(['ROLE_JOURNALIST']);
            $manager->persist($journalist);
        }
        
        for ($i=0; $i <4 ; $i++) { 
            $media = new Media();
            $media->setTitle($faker->text(200));
            $media->setDescription($faker->paragraph);
            $media->setAuthor($faker->name);
            $media->setDate($faker->dateTimeThisCentury);
            $media->setVideo('https://www.youtube.com/watch?v=GiUI1cUMa9I');
            $manager->persist($media);
        
        }
        $manager->flush();
    }

}
