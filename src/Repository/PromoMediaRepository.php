<?php

namespace App\Repository;

use App\Entity\PromoMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromoMedia|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoMedia|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoMedia[]    findAll()
 * @method PromoMedia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoMediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoMedia::class);
    }

    // /**
    //  * @return PromoMedia[] Returns an array of PromoMedia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoMedia
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
