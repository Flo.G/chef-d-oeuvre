<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200106124923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE journalist (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, age INT NOT NULL, nationality VARCHAR(255) NOT NULL, speciality VARCHAR(255) NOT NULL, img_path VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_CAF2FB9FE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE promo_media (id INT AUTO_INCREMENT NOT NULL, journalist_id INT DEFAULT NULL, video VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, goal INT NOT NULL, INDEX IDX_83059D9A34F59171 (journalist_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dons (id INT AUTO_INCREMENT NOT NULL, promo_media_id INT DEFAULT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, name_journalist VARCHAR(255) NOT NULL, INDEX IDX_E4F955FA2240F84D (promo_media_id), INDEX IDX_E4F955FAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, journalist_id INT DEFAULT NULL, date DATE NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, author VARCHAR(255) NOT NULL, video VARCHAR(255) NOT NULL, INDEX IDX_6A2CA10C34F59171 (journalist_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promo_media ADD CONSTRAINT FK_83059D9A34F59171 FOREIGN KEY (journalist_id) REFERENCES journalist (id)');
        $this->addSql('ALTER TABLE dons ADD CONSTRAINT FK_E4F955FA2240F84D FOREIGN KEY (promo_media_id) REFERENCES promo_media (id)');
        $this->addSql('ALTER TABLE dons ADD CONSTRAINT FK_E4F955FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C34F59171 FOREIGN KEY (journalist_id) REFERENCES journalist (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE promo_media DROP FOREIGN KEY FK_83059D9A34F59171');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C34F59171');
        $this->addSql('ALTER TABLE dons DROP FOREIGN KEY FK_E4F955FAA76ED395');
        $this->addSql('ALTER TABLE dons DROP FOREIGN KEY FK_E4F955FA2240F84D');
        $this->addSql('DROP TABLE journalist');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE promo_media');
        $this->addSql('DROP TABLE dons');
        $this->addSql('DROP TABLE media');
    }
}
