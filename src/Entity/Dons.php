<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DonsRepository")
 */
class Dons
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameJournalist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PromoMedia", inversedBy="Dons")
     */
    private $promoMedia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="dons")
     */
    private $User;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getNameJournalist(): ?string
    {
        return $this->nameJournalist;
    }

    public function setNameJournalist(string $nameJournalist): self
    {
        $this->nameJournalist = $nameJournalist;

        return $this;
    }

    public function getPromoMedia(): ?PromoMedia
    {
        return $this->promoMedia;
    }

    public function setPromoMedia(?PromoMedia $promoMedia): self
    {
        $this->promoMedia = $promoMedia;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
}
