<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoMediaRepository")
 */
class PromoMedia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $video;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $goal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Journalist", inversedBy="PromoMedia")
     */
    private $journalist;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dons", mappedBy="promoMedia")
     */
    private $Dons;

    public function __construct()
    {
        $this->Dons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGoal(): ?int
    {
        return $this->goal;
    }

    public function setGoal(int $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getJournalist(): ?Journalist
    {
        return $this->journalist;
    }

    public function setJournalist(?Journalist $journalist): self
    {
        $this->journalist = $journalist;

        return $this;
    }

    /**
     * @return Collection|Dons[]
     */
    public function getDons(): Collection
    {
        return $this->Dons;
    }

    public function addDon(Dons $don): self
    {
        if (!$this->Dons->contains($don)) {
            $this->Dons[] = $don;
            $don->setPromoMedia($this);
        }

        return $this;
    }

    public function removeDon(Dons $don): self
    {
        if ($this->Dons->contains($don)) {
            $this->Dons->removeElement($don);
            // set the owning side to null (unless already changed)
            if ($don->getPromoMedia() === $this) {
                $don->setPromoMedia(null);
            }
        }

        return $this;
    }
}
